import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FrankDriver {

    private static HashSet<String> totalWords = new HashSet<>(Arrays.asList("total", "final", "ending", "closing"));
    private static HashSet<String> endingWords = new HashSet<>(Arrays.asList("previous", "old", "open"));

    public static void main(String[] args) {
        FrankDriverTest.checkPhoneNumber();

        File directory = new File("documents");
        File[] files = directory.listFiles();
        if (files == null)
            System.exit(1);

        PrintWriter pw = null;
        StringBuilder sb = new StringBuilder();

        sb.append("PATH");
        sb.append(",");
        sb.append("TYPE");
        sb.append(",");
        sb.append("AMOUNT");
        sb.append(",");
        sb.append("OTHER");
        sb.append("\n");


        try {
            pw = new PrintWriter(new File("Frank, Kelli Text.csv"));

            for (File file : files) {
                try {
                    if (file.getPath().contains("._"))
                        continue;
                    Document doc = processFile(file);

                    DecimalFormat decim = new DecimalFormat("0.00");

                    sb.append(doc.getPath());
                    sb.append(", ");
                    sb.append(doc.getType());
                    sb.append(", ");

                    sb.append("$");
                    sb.append(decim.format(doc.getAmount()));
                    sb.append(", ");

                    if (doc.getType().equals("receipt")) {
                        sb.append("$");
                        sb.append(decim.format(Double.parseDouble(doc.getOther()))); // change
                    } else
                        sb.append(doc.getOther());

                    sb.append("\n");


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        pw.write(sb.toString());
        pw.close();
    }

    private static Document processFile(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file);
        Document doc = new Document(file.getAbsolutePath());

        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            String lowerCaseLine = line.toLowerCase();
            if (doc.getType().equals("n/a")) {
                // havent found type yet
                if (lowerCaseLine.contains("bank") || lowerCaseLine.contains("statement")) {
                    // bank statement
                    doc.setType("bank");
                } else if (lowerCaseLine.contains("receipt")) {
                    doc.setType("receipt");
                }
            }

            if (doc.getType().equals("receipt")) {
                if (lowerCaseLine.contains("total") && !lowerCaseLine.contains("subtotal") && doc.getAmount() < 0) {
                    // total but not subtotal, hasn't been found yet
                    doc.setAmount(getAmountFromLine(line));
                }
                if (lowerCaseLine.contains("change")) {
                    doc.setOther("" + getAmountFromLine(line));
                }
            } else {
                // bank
                if (doc.getOther().equals("n/a")) {
                    if (validatePhoneNumber(line) != null) { // todo need to parse out just number
                        doc.setOther(getPhoneNumber(line));
                    }
                }
                if (lowerCaseLine.contains("balance")) {
                    // need to see if total balance
                    // look for words such as total, final, ending

                    for (String word : line.split(" ")) {
                        if (totalWords.contains(word.toLowerCase())) {
                            doc.setAmount(getAmountFromLine(line));
                        }
                    }
                }
            }
        }

        if (doc.getAmount() == -1.0) {
            // did not find total balance, need to check again
            scanner = new Scanner(file);
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                String lowerCaseLine = line.toLowerCase();
                if (lowerCaseLine.contains("balance")) {
                    if (!lowerCaseLine.contains("previous") && !lowerCaseLine.contains("old") && !lowerCaseLine.contains("open")) {
                        doc.setAmount(getAmountFromLine(line));
                    }
                }
            }
        }

        if (doc.getType().equals("receipt") && doc.getOther().equals("n/a")) {
            doc.setOther("0.00");
        }

        System.out.println("PATH: " + doc.getPath());
        System.out.println("TYPE: " + doc.getType());
        System.out.println("AMOUNT: $" + doc.getAmount());
        System.out.println("OTHER: " + doc.getOther().trim() + "\n");

        return doc;
    }


    private static double getAmountFromLine(String line) {
        // check if line contains total

        // go through line to find total
        String original = line;

        line = line.replaceAll(" ", "").replaceAll(",", "");
        for (int i = 0; i < line.length(); i++) {
            char curr = line.charAt(i);
            String temp = original.substring(original.indexOf(curr));
            if (temp.indexOf(" ") < temp.indexOf(".") && temp.indexOf(" ") > 0) {
                // not dollar amount
                continue;
            }
            if (Character.isDigit(curr)) {
                String totalStr = "0";
                // number
                while ((Character.isDigit(curr) || (curr == '.')) && i < line.length()) {
                    curr = line.charAt(i);
                    totalStr += curr; // add onto string representation
                    i++;
                }
                return Double.parseDouble(totalStr);
            }

        }
        return -1;
    }

    // https://www.journaldev.com/641/regular-expression-phone-number-validation-in-java
    private static String validatePhoneNumber(String line) {
        line = ", " + line; // add beginning for matching
        //validate phone numbers of format "1234567890"
        String check = "(?s).*\\d{10}";
        if (line.matches(check)) return check.substring(check.indexOf('*') + 1);
            //validating phone number with -, . or spaces
        else if (line.matches(check = "\\*[-.\\s]\\d{3}[-.\\s]\\d{3}[-.\\s]\\d{4}"))
            return check.substring(check.indexOf('*') + 1);
        else if (line.matches(check = "(?s).*[-.\\s]\\d{3}[-.\\s]\\d{3}[-.\\s]\\d{4}\\b.*"))
            return check.substring(check.indexOf('*') + 1);

        else if (line.matches(check = "(?s).*\\d{3}[-.\\s]\\d{3}[-.\\s]\\d{4}"))
            return check.substring(check.indexOf('*') + 1);
        else if (line.matches(check = "(?s).*\\d{3}[-.\\s]\\d{3}[-.\\s]\\d{4}\\b.*"))
            return check.substring(check.indexOf('*') + 1);
            //validating phone number with extension length from 3 to 5
        else if (line.matches(check = "(?s).*\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}"))
            return check.substring(check.indexOf('*') + 1);
        else if (line.matches("(?s).*\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}\\b.*"))
            return check.substring(check.indexOf('*') + 1);
            //validating phone number where area code is in braces ()
        else if (line.matches(check = "(?s).*\\(\\d{3}\\)-\\d{3}-\\d{4}"))
            return check.substring(check.indexOf('*') + 1);
        else if (line.matches(check = "(?s).*\\(\\d{3}\\)-\\d{3}-\\d{4}\\b.*"))
            return check.substring(check.indexOf('*') + 1);
            //return false if nothing matches the input
        else return null;

    }

    public static String getPhoneNumber(String line) {
        line = line.trim();
        Pattern p = Pattern.compile("1?[(\\- ]?[0-9]{3}\\)?[0-9\\-[\\s?]]{7,20}");
        Matcher m = p.matcher(line);
        String possible = "";
        while (m.find()) {
            possible = m.group();
        }
        if (possible.contains("  "))
            return getPhoneNumber(line.substring(1));

        possible = possible.trim().replaceAll("-", "");
        possible = possible.trim().replaceAll("\\(", "");
        possible = possible.trim().replaceAll("\\)", "");
        possible = possible.trim().replaceAll(" ", "");

        switch (possible.length()) {
            case 10:
                possible = possible.substring(0, 3) + '-' + possible.substring(3, 6) + '-' + possible.substring(6);
                break;

            case 11:
                possible = possible.substring(1);
                possible = possible.substring(0, 3) + '-' + possible.substring(3, 6) + '-' + possible.substring(6);
                possible = "1-" + possible;
                break;
        }
        return possible.trim();
    }

}

class Document {
    private String path, type, other;
    private double amount;

    Document(String path) {
        this.path = path;
        type = "n/a";
        amount = -1.0;
        other = "n/a";
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }
}


class FrankDriverTest {

    static void checkPhoneNumber() {
        String opt1 = "123-456-7890";
        String opt2 = "1-123-456-7890";
        String ans = FrankDriver.getPhoneNumber("1234567890");
        assertEquals(opt1, ans);
        ans = FrankDriver.getPhoneNumber("11234567890");
        assertEquals(opt2, ans);

        ans = FrankDriver.getPhoneNumber("123-456-7890");
        assertEquals(opt1, ans);
        ans = FrankDriver.getPhoneNumber("1-123-456-7890");
        assertEquals(opt2, ans);

        ans = FrankDriver.getPhoneNumber("(123)456-7890");
        assertEquals(opt1, ans);
        ans = FrankDriver.getPhoneNumber("1(123)456-7890");
        assertEquals(opt2, ans);
        ans = FrankDriver.getPhoneNumber("(123) 456-7890");
        assertEquals(opt1, ans);
        ans = FrankDriver.getPhoneNumber("1 (123) 456 - 7890");
        assertEquals(opt1, ans);

        ans = FrankDriver.getPhoneNumber("123 456 7890");
        assertEquals(opt1, ans);
        ans = FrankDriver.getPhoneNumber("1 123 456 7890");
        assertEquals(opt2, ans);

        ans = FrankDriver.getPhoneNumber("8791          123 456 7890");
        assertEquals(opt1, ans);
    }

}